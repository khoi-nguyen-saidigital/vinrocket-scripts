
-- Get all user role
select distinct acl.negative, ct.INTERNALCODE, ur.p_code, ug.p_uid from aclentries  acl
JOIN usergroups  ug ON acl.principalpk = ug.PK
JOIN composedtypes  ct ON ct.PK = acl.itempk
JOIN userrights  ur ON ur.PK = acl.permissionPK
where ug.p_uid like 'merchant%'
AND ur.p_code != 'changerights'
AND ug.p_uid = 'merchantcenterbookingrole'
AND ug.p_uid not in ('merchantownerrole', 'merchantownerinternalrole')
AND acl.negative=1


