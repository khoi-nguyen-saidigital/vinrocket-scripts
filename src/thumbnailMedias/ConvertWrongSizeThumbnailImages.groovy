package thumbnailMedias

import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
sessionService = spring.getBean "sessionService"

stagedProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Staged")

CATALOG_VERSION = stagedProductCatalog.pk
ACTION = 'fresh_product_details_thumbnail_format'

conversionFormat = flexibleSearchService.search("SELECT {pk} FROM {ConversionMediaFormat} where {qualifier} = '" + ACTION + "'").result.get(0)

findOutDateMedia().each {
    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel()
    item.action = ACTION
    reason = ""
    try {
        media = it;
        mediaCode = media.code
        item.productCode = mediaCode
        mediaContainer = media.mediaContainer
        conversionGroup = mediaContainer.conversionGroup
        supportedFormats = conversionGroup.getSupportedFormats()

        modelService.remove(media)
        convertedMedias = findConvertedMedias(mediaContainer)
        modelService.removeAll(convertedMedias)

        reason = "REMOVED"
        supportedFormats.each {
            if (it.qualifier.equals(ACTION)) {
                reason = "CONVERTED"
                mediaConversionService.convertInTransaction(mediaContainer, it)
            }
        }


    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg != null && errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason = "ERROR | " + errorMsg
        log(e)
    }

    item.reason = reason;
    modelService.save(item)
    log("FINISH " + ACTION + " for image " + item.productCode + " with reason " + item.reason)
}

def findConvertedMedias(mediaContainer) {
    Map<String, Object> params = new HashMap<>();
    params.put("container", mediaContainer)
    params.put("format", conversionFormat)
    query = new FlexibleSearchQuery("SELECT {PK} FROM {ProductMedia} WHERE {MediaContainer} = ?container and {MediaFormat} = ?format", params)
    return flexibleSearchService.search(query).result
}
def findOutDateMedia() {
    queryStr = "select {pk} from {ProductMedia as pm JOIN ConversionMediaFormat as mc ON {pm.mediaformat} = {mc.pk}} where {pm.catalogversion} = 8796093153881 and {pm.mediaformat} is not null AND {pm.mediacontainer} is not null AND  {mc.qualifier} = '"+ACTION+"' and {creationtime} < to_date('13/07/2017', 'dd/mm/yyyy')"
    query = new FlexibleSearchQuery(queryStr)
    query.count = 20000
    log(query)
    result = flexibleSearchService.search(query).result
    log(ACTION + " Found " + result.size() + " items")
    return result
}

def log(message) {
//    println message
    log.info(message)
}