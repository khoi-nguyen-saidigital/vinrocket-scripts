package thumbnailMedias

import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
sessionService = spring.getBean "sessionService"

stagedProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Staged")

CATALOG_VERSION = stagedProductCatalog.pk
ACTION = 'fashion_product_details_thumbnail_format'

findOutDateMedia().each {
    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel()
    item.action = "REVERT_" + ACTION
    reason = ""
    try {
        media = it;
        mediaCode = media.code
        item.productCode = mediaCode
        media.mediaContainer = media.original.mediaContainer
        modelService.save(media)
        reason = "DONE"

    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg != null && errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason = "ERROR | " + errorMsg
        log(e)
    }

    item.reason = reason;
    modelService.save(item)
    log("FINISH REVERT " + ACTION + " for image " + item.productCode + " with reason " + item.reason)
}

def findOutDateMedia() {
    queryStr = "select {pk} from {ProductMedia as pm JOIN ConversionMediaFormat as mc ON {pm.mediaformat} = {mc.pk}} where {pm.catalogversion} = 8796093153881 and {pm.mediaformat} is not null AND {pm.mediacontainer} is null AND  {mc.qualifier} = '"+ACTION+"' and {creationtime} < to_date('13/07/2017', 'dd/mm/yyyy')"
    query = new FlexibleSearchQuery(queryStr)
    query.count = 100000
    log(query)
    result = flexibleSearchService.search(query).result
    log(ACTION + " Found " + result.size() + " items")
    return result
}

def log(message) {
//    println message
    log.info(message)
}