package support

import com.vingroup.rocket.core.model.MerchantOfferModel
import com.vingroup.rocket.core.services.price.impl.VinrocketDefaultPriceService
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.europe1.constants.Europe1Constants
import de.hybris.platform.europe1.enums.UserPriceGroup
import de.hybris.platform.europe1.jalo.PriceRow
import de.hybris.platform.europe1.model.PriceRowModel
import de.hybris.platform.jalo.c2l.Currency
import de.hybris.platform.jalo.order.price.PriceInformation
import org.apache.commons.collections.CollectionUtils

catalogVersionService = spring.getBean "catalogVersionService"
merchantOfferService = spring.getBean "merchantOfferService"
vinPriceService = spring.getBean "vinPriceService"
priceService = spring.getBean "priceService"

offerCode = "duyendeal1_CUK"
catalogVersionService.setSessionCatalogVersion("adayroiProductCatalog", "Online")
offer = findOffer(offerCode)
sellingPrice = vinPriceService.getPriceDataForProduct(offer, true)
info = priceService.getPriceInformationsForProduct(offer)

println sellingPrice
println info

def findOffer(offerCode) {
    return merchantOfferService.getOfferForCode(offerCode)
}

public List<PriceInformation> getPriceInformationsForProduct(final ProductModel product)
{
    String region = "GLOBAL";


    final String cacheKey = product.getPk().getLongValueAsString() + "_" + product.getCode() + "_" + region;
    //get from cache (cached only 30secs)
    final List<PriceInformation> priceInformations = getFromCache(cacheKey);
    if (priceInformations != null)
    {
        log.debug("getting price from CACHE for " + product.getCode() + " for the region " + region);
        return priceInformations;
    }

    log.debug("Getting Price for Product=" + product.getCode() + " for the region " + region);
    final CurrencyModel currency = commerceCommonI18NService.getCurrentCurrency();
    final Date currentTime = timeService.getCurrentTime();
    final Currency curr = getModelService().getSource(currency);

    List<PriceRowModel> pricerows = getPriceRowsForProductCode(product.getCode(), product.getCatalogVersion(), region);

    if (!(product instanceof MerchantOfferModel) && CollectionUtils.isEmpty(pricerows))
    {
        pricerows = (List<PriceRowModel>) product.getEurope1Prices();
    }

    if (!region.equalsIgnoreCase("GLOBAL") && CollectionUtils.isEmpty(pricerows))
    {
        log.debug("No Price found for the region:" + region + " (product)" + product.getCode() + ": getting from global price.");
        pricerows = getPriceRowsForProductCode(product.getCode(), product.getCatalogVersion(), "GLOBAL");
    }

    // select the Pricerow which has valid range for today date.
    pricerows = filterPriceByDateRange(pricerows, currentTime);

    // VIN-3920 When there are multiple active price row for that offer, mark down price will be more prioritized than non-mark down price.
    final List<PriceRowModel> markedDownPriceRows = filterMarkedDownPrices(pricerows);
    if (!CollectionUtils.isEmpty(markedDownPriceRows))
    {
        pricerows = markedDownPriceRows;
    }
    PriceRowModel price = null;
    if (CollectionUtils.isNotEmpty(pricerows))
    {
        if (pricerows.size() == 1)
        {
            price = pricerows.get(0);
        }
        else if (pricerows.size() > 1)
        {
            // get the price with latest start date
            price = Collections.max(pricerows, VinrocketDefaultPriceService.PricerowComparator.INSTANCE);
        }
    }

    if (price != null)
    {
        final List<PriceInformation> priceinfo = new ArrayList<>();
        final PriceRow prow = getModelService().getSource(price);
        priceinfo.add(createPriceInfo(prow, curr));
        //put into cache
        putIntoCache(cacheKey, priceinfo);
        return priceinfo;
    }
    else
    {
        //log.error("Cannot find price for product: " + product.getCode());
    }

    return Collections.emptyList();
}