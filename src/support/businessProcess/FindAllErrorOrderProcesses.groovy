import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

query = "select {p.PK} from {OrderProcess! as p JOIN ENumerationValue as s ON {s.pk} = {p.state} \n" +
        "JOIN Order as o ON {p.order} = {o.pk}}\n" +
        "where {s.code} = 'ERROR' " +
        "AND {p.creationtime} > to_date('25/11/2017', 'dd/mm/yyyy') order by {modifiedtime} desc"

flexibleSearchService.search(new FlexibleSearchQuery(query)).result.each {
    processCode = it.code
    tasks = it.taskLogs
    lastTask = tasks[tasks.size() - 1]
    println processCode + "|" + lastTask.actionId + "|" + it.modifiedtime
}
