package support.businessProcess

import de.hybris.platform.payment.model.PaymentTransactionModel
import org.apache.commons.collections.CollectionUtils

orderCode = "7126002510"
updatePaymentTransaction(orderCode)

def updatePaymentTransaction(orderCode) {
    order = vinOrderService.getSaleOrderModelByCode(orderCode)

    vinIdPaymentTransactions = new ArrayList<>()
    order.paymentTransactions.each {
        PaymentTransactionModel p = it
        if (p.paymentProvider.equals("vinIDRedeem")) {
            vinIdPaymentTransactions.push(p)
        }

    }
    if (CollectionUtils.isNotEmpty(vinIdPaymentTransactions)) {
        refundItem = redeemItem = -1;
        for (int i = vinIdPaymentTransactions.size() -1 ; i >= 0; i--) {
            PaymentTransactionModel p = vinIdPaymentTransactions.get(i)
            refundRequest = p.refundRequest
            p.entries.each {
                if (refundItem == -1 && refundRequest != null && it.transactionStatus.equals("STARTED")) {
                    refundItem = i;
                }

                if (redeemItem == -1 && refundRequest == null && it.transactionStatus.equals("ACCEPTED")) {
                    redeemItem = i
                }
            }

            if (refundItem != -1 && redeemItem != -1) {

                break;
            }
        }
        if (refundItem != -1 && redeemItem != -1) {
            vinIdPaymentTransactions.get(refundItem).entries.each {
                it.transactionStatus = "ACCEPTED"
                modelService.save(it)
            }

            vinIdPaymentTransactions.get(redeemItem + 1).entries.each {
                it.transactionStatus = "STARTED"
                modelService.save(it)
            }
        }

    }
}
