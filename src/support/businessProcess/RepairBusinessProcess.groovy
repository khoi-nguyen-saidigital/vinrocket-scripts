package support.businessProcess

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

processCode = "order-process-7127004000-1513921087268"
action = "handleLockAmount"

findProcessByCode(processCode).each {
    businessProcessService.restartProcess(it, action)
}
def findProcessByCode(code) {
    query = "SELECT {PK} FROM {BusinessProcess} WHERE {code} = ?code"
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query)
    flexibleSearchQuery.addQueryParameter("code", code)
    flexibleSearchService.search(flexibleSearchQuery).result
}