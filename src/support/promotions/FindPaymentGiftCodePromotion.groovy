import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import groovy.json.JsonSlurper

String query = "select {pk} from {PromotionSourcerule as p JOIN EnumerationValue as t ON {t.pk} = {p.adayroisharecosttype}} where {adayroisharecosttype} is not null AND {t.code} = 'PAYMENT_GIFTCODE'"
FlexibleSearchQuery q = new FlexibleSearchQuery(query)
flexibleSearchService.search(q).result.each {

    promotion = it;
    coupon = ""
    fromDate = ""
    toDate = ""
    condition = new JsonSlurper().parseText(it.conditions)
    condition.each {
        if (it.definitionId == "y_qualifying_coupons") {
            it.parameters.each {
                coupon = it.value.value[0]

//                println promotion.code + "|"+ value[0] + "|" + promotion.status
            }
        }
        else if (it.definitionId == "vin_combined_timer") {
            fromDate = it.parameters.fromDate.value
            toDate = it.parameters.toDate.value
        }
    }
    println promotion.code + "|" + coupon + "|" + fromDate + "|" + toDate


}