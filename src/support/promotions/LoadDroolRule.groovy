package support.promotions

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

flexibleSearchService = spring.getBean "flexibleSearchService"

droolRules = findDroolRule()
droolRules.each {
    promotion = it
    promotionCodes = []
    ruleContent = it.ruleContent
    if (ruleContent.contains("PRI344080")) {
        promotionCodes.add(promotion.code)
    }


    promotionCodes.each {
        println it
    }
}

def findDroolRule() {
    query = "SELECT {PK} from {DroolsRule}"
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query)
    flexibleSearchService.search(flexibleSearchQuery).result
}


