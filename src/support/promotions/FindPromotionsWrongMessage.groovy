package support.promotions

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import groovy.json.JsonSlurper

/**
 * Created by Khoi Nguyen on 7/10/2017.
 */
flexibleSearchService = spring.getBean "flexibleSearchService"
modelService = spring.getBean "modelService"
findPromotions().each {
    if (it != null) {
        promotion = it;
        println promotion.conditions
        if (promotion.amountBudget != null || promotion.quantityBudget != null || promotion.appliedBudget != null) {
            println "========"
            println promotion.code + " === " + promotion.status
            println promotion.amountBudget
            println promotion.quantityBudget
            println promotion.appliedBudget
        }

    }
}

def findPromotions() {
    promotionQuery = "SELECT {pk} FROM {PromotionSourceRule as p}"
    FlexibleSearchQuery query = new FlexibleSearchQuery(promotionQuery)
//    query.count = 500
    flexibleSearchService.search(query).result
}
