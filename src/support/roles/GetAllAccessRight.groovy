package support.roles

import de.hybris.platform.jalo.security.AccessManager
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

flexibleSearchService = spring.getBean "flexibleSearchService"
/**
 * Created by Khoi Nguyen on 7/15/2017.
 */
def getRole(roleName) {
    query = "SELECT {pk} from {BackofficeRole} where {uid} = 'merchantcenterofferviewerrole'"
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query)
    flexibleSearchService.search(flexibleSearchQuery).result
}

role = getRole("kjkj")

AccessManager accessManager = AccessManager.getInstance();
println accessManager.getAllGlobalPositivePermissions(role)
