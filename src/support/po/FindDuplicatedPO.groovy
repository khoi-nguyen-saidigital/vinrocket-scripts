package support.po

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

flexibleSearchService = spring.getBean "flexibleSearchService"

count = 0;
findDuplicatedPO().each {
    if (it.shipment == null) {
        println it.code + "|" + it.pk + "|" + it.status
        it.code = it.code + "-changed"
        modelService.save(it)
    }
}
def findDuplicatedPO() {
    FlexibleSearchQuery query = new FlexibleSearchQuery("select distinct{v1.pk}\n" +
            "from {vinpurchaseorder as v1 join vinpurchaseorder as v2 on {v1.code} = {v2.code}}\n" +
            "WHERE {v1.PK} != {v2.PK} AND {v1.code} like '711%' AND {v1.code} not like '%-changed'")
    return flexibleSearchService.search(query).result
}
