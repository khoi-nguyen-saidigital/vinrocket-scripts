package support

import com.vingroup.rocket.core.model.VinPurchaseOrderModel

vinPurchaseOrderService = spring.getBean "vinPurchaseOrderService"
modelService = spring.getBean "modelService"
/**
 * Created by Khoi Nguyen on 7/4/2017.
 */
PoIds = ["706864816701"]
PoIds.each {
    poId = it;
    po  = vinPurchaseOrderService.findPOByCode(poId)
    po.consignmentEntries.each {
        consignmentEntry = it;
        orderEntryBasedPrice = consignmentEntry.orderEntry.basePrice
        println orderEntryBasedPrice
        consignmentEntry.itemEntries.each {
            println it.baseOfferPrice
            if (it.baseOfferPrice == null) {
                it.baseOfferPrice = orderEntryBasedPrice
                modelService.save(it)
            }
        }
    }
}
