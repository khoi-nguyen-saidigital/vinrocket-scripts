package support

import com.vingroup.rocket.facades.order.data.VinRefundData

/**
 * Created by Khoi Nguyen on 6/27/2017.
 */

vinIdRedeemService = spring.getBean "vinIdRedeemService"

vinId = "8888200281117582"
originalInvoiceNo = "710734241001829272"
invoiceNo = "ref" + originalInvoiceNo

VinRefundData vinRefundData = new VinRefundData()
vinRefundData.originalInvoiceNo = originalInvoiceNo
vinRefundData.invoiceNo = invoiceNo
vinRefundData.vinId = vinId
vinRefundData.refundAmount = 236

vinIdRedeemService.refund(vinRefundData)
