package support

import com.vingroup.rocket.core.util.VinDiscountValue
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService

/**
 * Created by Khoi Nguyen on 7/1/2017.
 */

def orderCode = "7077879224"

flexibleSearchService = spring.getBean "flexibleSearchService"
costAllocationService = spring.getBean "costAllocationService"

order = getAbstractOrder(orderCode)


final List<VinDiscountValue> globalDiscountValues = VinDiscountValue
        .filterVinDiscountValuesWithoutGiftCodePayment(order.getGlobalDiscountValues())

globalDiscountValues.each {
    println it
    println it.getConsumedEntryNumber_Quantity()
}

def getAbstractOrder(orderCode) {
    FlexibleSearchQuery flexibleQuery = new FlexibleSearchQuery("SELECT {pk} FROM {AbstractOrder} where {code} = ?code")
    flexibleQuery.addQueryParameter("code", orderCode)
    flexibleSearchService.search(flexibleQuery).result.get(0)
}
