package support

vinMerchantOfferService = spring.getBean "vinMerchantOfferService"

catalogVersionService = spring.getBean "catalogVersionService"
merchantOfferService = spring.getBean "merchantOfferService"
vinPriceService = spring.getBean "vinPriceService"
priceService = spring.getBean "priceService"

offerCode = "PRI33976_CHK"
catalogVersionService.setSessionCatalogVersion("adayroiProductCatalog", "Online")
offer = findOffer(offerCode)
availableStock = vinMerchantOfferService.getAvailableStockForOffer(offer)

def findOffer(offerCode) {
    return merchantOfferService.getOfferForCode(offerCode)
}

