import com.vingroup.rocket.core.enums.VatCalculationMethod
import com.vingroup.rocket.core.model.*
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.order.AbstractOrderEntryModel
import de.hybris.platform.core.model.order.OrderEntryModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.variants.model.GenericVariantProductModel

findOrders().each {
    orderCode = it.code
    it.entries.each {
        product = it.product
        contractModel = it.appliedMerchantContractModel
        println orderCode + "|" + product.code + "|" + contractModel
        if (contractModel == null) {
            storeContextData(it)
            modelService.save(it)
        }
    }
}

def findOrders() {
    query = "select distinct{o.pk} from {OrderEntry as oe JOIN Order as o ON {oe.order} = {o.pk}} where {o.code} like '705%' and {oe.appliedMerchantContractModel} is null"
    FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query)
    searchQuery.count = 50
    flexibleSearchService.search(searchQuery).result
}


private void storeContextData(final OrderEntryModel entry) {
    try {

        //1. effective merchant Contract TODO remove, should use specifed commission rate, since the effective contract could be changed
        final MerchantOfferModel offer = (MerchantOfferModel) entry.getProduct();
        final CommissionContractDetailModel commissionContractDetail = merchantVendorService
                .getEffectiveContractDetailForMerchantAndProduct(offer.getMerchant().getCode(), offer.getProduct());
        if (commissionContractDetail != null) {
            entry.setCurrentEffectiveMerchantContract(commissionContractDetail.getMerchantContract());
            entry.setCurrentCommissionContractDetail(commissionContractDetail);
        }

        if (commissionContractDetail != null) {
            //2.Commission value
            entry.setAppliedCommissionValue(commissionContractDetail.getCommissionRate());
            //3.Commission type
            entry.setCommissionType(commissionContractDetail.getCommissionType());
            //4. appliedMerchantContractModel
            entry.setAppliedMerchantContractModel(commissionContractDetail.getMerchantContract().getContractBusinessModel());
            //5. vatout
            populateVatout(entry, offer);

            //6. VatIn
            if (VatCalculationMethod.DEDUCT.equals(offer.getMerchant().getVatCalculationMethod())) {
                if (offer.getVatIn() != null) {
                    entry.setAppliedVatIn(offer.getVatIn());
                } else {
                    entry.setAppliedVatIn(0d);
                    
                    error("VAT IN is null for offer" + offer.getCode());
                }
            } else {
                entry.setAppliedVatIn(0d);
            }

            //12. require serial
            entry.setWasSerialRequired(offer.getProduct().getRequireSerial());

            //13. require booking
            entry.setWasBookingRequired(offer.getIsBookingRequired());

            final VinStoreRewardRateModel vinIDRewardRate = offer.getVinStoreRewardRate();
            if (vinIDRewardRate != null) {
                //14. appliedVinIDRewardRate
                entry.setAppliedVinIDRewardRate(vinIDRewardRate.getRate());
                //15. applied VinID store ID
                entry.setAppliedVinIDRewardStoreId(vinIDRewardRate.getStoreId());
                //16. applied vinID Reward Terminal ID
                entry.setAppliedVinIDRewardTerminalId(vinIDRewardRate.getTerminalId());
            }

            //17. applied Revenue RecognitionAt
            entry.setAppledRevenueRecognitionAt(commissionContractDetail.getMerchantContract().getRevenueRecognitionAt());

            //18. applied threshold for fresh picking
            entry.setAppliedThresholdForFreshPicking(offer.getEffectiveThreshold());

            //19. voucher expiration date
            entry.setVoucherExpirationDate(offer.getExpirationDate());

            //20. voucher available date
            entry.setVoucherExpirationDate(offer.getVoucherAvailableDate());

            //7. mapped SAP Article
            final SapProductMasterModel mapping = vinSAPService.findSAPMappingArticle(offer);
            if (mapping != null) {
                //8. mappedBaseArticleId
                entry.setMappedSAPBaseArticleId(mapping.getSapBaseArticleId());
                //9. mapped quantity per unit
                entry.setMappedSAPQuantityPerUnit(mapping.getQuantityPerUnit());
                //10. mapped SAP source system
                entry.setMappedSAPSource(mapping.getSapSource());
                //11. mapped UOM
                entry.setMappedUOM(mapping.getUom());
            }
        }
    }

    catch (Exception t) {
        error("Error at storeContextData:", t);
    }
}

def error(msg) {
    println "ERROR: " + msg
}

private void populateVatout(AbstractOrderEntryModel entry, MerchantOfferModel offer) {
    //get directly from offer
    if (offer.getVatOutClass() != null)
    {
        entry.setAppliedVatOut(offer.getVatOutClass().getRate());
        entry.setAppliedVatOutClass(offer.getVatOutClass().getTaxClass());
        return;
    }

    //get from product or variant
    ProductModel product = offer.getProduct();
    if (product.getVatOutClass() != null)
    {
        entry.setAppliedVatOut(product.getVatOutClass().getRate());
        entry.setAppliedVatOutClass(product.getVatOutClass().getTaxClass());
        return;

    }

    ProductModel baseProduct = null;
    //get from base product
    if (product instanceof GenericVariantProductModel){
        baseProduct = ((GenericVariantProductModel) product).getBaseProduct();
        if (baseProduct != null && baseProduct.getVatOutClass() != null){
            entry.setAppliedVatOut(baseProduct.getVatOutClass().getRate());
            entry.setAppliedVatOutClass(baseProduct.getVatOutClass().getTaxClass());
            return;
        }
    }

    //get from leaf category
    ProductModel rootProduct = baseProduct != null? baseProduct : product;
    CommissionCategoryModel leafCat = rootProduct.getCommissionCategory();
    if (leafCat.getVatOutClass() != null){
        entry.setAppliedVatOut(leafCat.getVatOutClass().getRate());
        entry.setAppliedVatOutClass(leafCat.getVatOutClass().getTaxClass());
        return;
    }

    //get from category thread
    List<CategoryModel> catThread =  leafCat.getSupercategories();
    for(CategoryModel cat : catThread ){
        if (cat instanceof CommissionCategoryModel){
            VatoutClassModel vatoutClassModel = ((CommissionCategoryModel) cat).getVatOutClass();
            if ( vatoutClassModel != null){
                entry.setAppliedVatOut(vatoutClassModel.getRate());
                entry.setAppliedVatOutClass(vatoutClassModel.getTaxClass());
                return;
            }
        }
    }
}