package support

import de.hybris.platform.core.enums.OrderStatus

/**
 * Created by Khoi Nguyen on 7/1/2017.
 */

def orderCode = "7104000000"


orderService = spring.getBean "vinOrderService"
order = orderService.getSaleOrderModelByCode(orderCode)

status = OrderStatus.CS_CONFIRMED_WAITING_FOR_LOGISTICS_TO_PROCESS

consignments = order.consignments
consignments.each {
    consignment = it;
    consignmentEntries = consignment.consignmentEntries
    consignmentEntries.each {
        consignmentEntry = it
        itemEntries = consignmentEntry.itemEntries
        itemEntries.each {
            it.status = status
            modelService.save(it)
        }

        consignmentEntry.status = status
        consignmentEntry.shippedQuantity = null
        consignmentEntry.failedDeliveryQuantity = null
        consignmentEntry.actualPickedQuantity = null


        modelService.save(consignmentEntry)
    }

}
