import com.google.gson.Gson
import com.vingroup.rocket.core.binlocation.dto.SendConfirmOrderRequest
import com.vingroup.rocket.core.util.OrderStatusConstant
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import groovy.json.JsonSlurper

orderCode = "7129006600"
String q = "SELECT {PK} FROM {VinIntegrationStatus as v JOIN EnumerationValue as target ON {target.pk} = {v.targetSystem}} WHERE {target.code} = ?code" +
        " AND {v.orderNumber} = ?order order by {creationtime}"
FlexibleSearchQuery query = new FlexibleSearchQuery(q)
query.addQueryParameter("code", "BIN_LOCATION")
query.addQueryParameter("order", orderCode)
query.count = 10
flexibleSearchService.search(query).result.each {
    response = it.response
    o = new JsonSlurper().parseText(response)
    println "=== " + it.creationtime + " === " + it.status + " === " + o.MessageInfo.Code
    request = it.request
    Gson g = new Gson()

    SendConfirmOrderRequest r = g.fromJson(request, SendConfirmOrderRequest.class)

    r.getPOInfos().each {
        poStatus = OrderStatusConstant.getStatusByNumber(Long.parseLong(it.POStatus))
        poCode = it.PONumber
        it.getProducts().each {
            product = it;
            productCode = product.getProductCode()
            productName = product.getProductName()
            productQuantity = product.getOrderQuantity()
            productStatus = OrderStatusConstant.getStatusByNumber(Long.parseLong(product.getPOProductStatus()))
            println poCode + "|" + poStatus.code + "|" + productStatus + "|" + productCode + "|" + productName + "|" + productQuantity
        }
    }


}

