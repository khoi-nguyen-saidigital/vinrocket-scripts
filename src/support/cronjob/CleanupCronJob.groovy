import de.hybris.platform.cronjob.model.CronJobModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

query = "select {pk}  from {CronJob} where {active} = 1 and {job} NOT IN ( 8796093055476, 8796095414772, 8796093579764,8797010559476,8796095021556,8796093612532,8796095382004) and {starttime} > to_date('01/12/2017', 'dd/mm/yyyy')"
flexibleSearchService.search(query).result.each {
    CronJobModel c = it;
    println it.code + "|" + c.status
    println c.triggers.get(0).cronExpression
    println c.timeTable
    println "==========="
}