package support

import de.hybris.platform.mediaconversion.enums.ConversionStatus
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService

/**
 * Created by Khoi Nguyen on 7/8/2017.
 */


flexibleSearchService = spring.getBean "flexibleSearchService"

products = findProcessedProducts()
log("HAS CONVERTED TOTALLY " + products.size() + " PRODUCTS")
notConvertedProds = []
products.each {
    p = it
    p.galleryImages.find {
        if (it.conversionStatus != ConversionStatus.CONVERTED) {
            notConvertedProds.add(p.code)
            return true;
        }
        return false;
    }
}

log("PRODUCTS HAS BEEN CONVERTED UNSUCCESSFULLY " + notConvertedProds.size())
notConvertedProds.each {
    log(it)
}


def log(mes) {
    println mes
//    log.info(mes)
}
def findProcessedProducts() {
    flexibleQuery = "SELECT distinct{product} from {ProcessedProductJobItem} where {action} = 'GROOVY_CONVERT_MEDIA_PRODUCTION'"
    FlexibleSearchQuery query = new FlexibleSearchQuery(flexibleQuery)
    query.count = 100
    flexibleSearchService.search(query).result
}
