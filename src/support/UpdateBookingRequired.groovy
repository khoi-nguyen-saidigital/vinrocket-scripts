flexibleSearchService = spring.getBean "flexibleSearchService"
modelService = spring.getBean "modelService"

offers = flexibleSearchService.search("select {m.pk} from {MerchantOffer as m JOIN Product as p ON {m.product} = {p.pk}} WHERE {m.isBookingRequired} = 0 and {p.isbookingrequired} = 1").result
int total = 0
int error = 0
offers.each {
    total ++;
    try {
        println it.code
        it.isBookingRequired = true
        modelService.save(it)


    } catch (Exception e) {
        error++

        println e
    }
}

println "TOTAL = " + total
println "ERROR = " + error

