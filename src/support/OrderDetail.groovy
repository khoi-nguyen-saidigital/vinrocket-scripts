package support

import com.vingroup.rocket.core.model.MerchantOfferModel
import com.vingroup.rocket.core.util.VinDiscountValue
import de.hybris.platform.core.model.order.AbstractOrderEntryModel
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.core.model.product.ProductModel
import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.lang3.BooleanUtils

/**
 * Created by Khoi Nguyen on 7/1/2017.
 */

//PRI1260427_M-R7N
def orderCode = "7118800361"


vinCartService = spring.getBean "vinCartService"
orderService = spring.getBean "vinOrderService"
vinCostAllocationService = spring.getBean "vinCostAllocationService"
vinMerchantOfferService = spring.getBean "vinMerchantOfferService"

//order = vinCartService.getCartForCode(orderCode)
order = orderService.getSaleOrderModelByCode(orderCode)
printCsTicket()
def printCsTicket() {
    order.tickets.each {
        if (it.ticketID == "00527441") {
            println it.description
        }

    }
}
def printDiscountInfo(OrderModel order) {
    println vinOrderService.getGiftCodeAmount(order)
    println vinCostAllocationService.getGiftCodeDiscounts(order, "")
    List<VinDiscountValue> allDiscountValues = new ArrayList<>();
    allDiscountValues.addAll(VinDiscountValue.filterVinDiscountValues(order.getGlobalDiscountValues()));
    allDiscountValues.each {
        println it
        println it.getCostShareType()
        println it.getAppliedGiftCode()
    }
}
//order.entries.each {
//    println it.product.code + "|" + it.quantity + "|" + it.basePrice
//}
//coupons = order.getAppliedCouponCodes()
//vinCostAllocationService.getConsumedEntries(coupons[0], order)
//List<VinDiscountValue> globalDiscountValue = VinDiscountValue.filterVinDiscountValues(order.getGlobalDiscountValues());
//globalDiscountValue.each {
//    println it.consumedEntryNumber_Quantity
//    println it.appliedValue
//
//}

//sum = 0;
//order.itemEntries.each {
//    if (it.finalPrice != it.baseOfferPrice) {
//        println it.baseOfferPrice + "|" + it.finalPrice
//        sum ++
//    }
////    if (it.finalPrice < 0) {
////        println it.vinDiscountValues
////    }
////    println it.baseOfferPrice
//
//}
//
//println sum