import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel

flexibleSearchService = spring.getBean "flexibleSearchService"
product = flexibleSearchService.search("SELECT {PK} from {Product!} where {code} = 'PRI404044'").result.get(0);
product.features.each {
    classificationAttribute = it.classificationAttributeAssignment.classificationAttribute
    println "CLASSIFICATION ATTRIBUTE CODE = " + classificationAttribute.code + " PK = "+classificationAttribute.pk
    classificationAttribute.classes.each {
        if (it != null) {
            println it.code
        } else {
            println it
        }
    }

    println "===================="
}