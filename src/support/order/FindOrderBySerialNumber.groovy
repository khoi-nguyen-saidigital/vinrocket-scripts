import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

serial = "000189374289374257"
FlexibleSearchQuery query = new FlexibleSearchQuery("select {pk} from {ItemEntry} where {serialNumber} = ?serial ")
query.addQueryParameter("serial", serial)

flexibleSearchService.search(query).result.each {
    println it
    println it.consignmentEntry.orderEntry.product.getEffectiveDate()
    println it.consignmentEntry.orderEntry.product.getExpirationDate()
}

