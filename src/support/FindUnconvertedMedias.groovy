package support

import de.hybris.platform.mediaconversion.enums.ConversionStatus
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

/**
 * Created by Khoi Nguyen on 6/30/2017.
 */

flexibleService = spring.getBean "flexibleSearchService"

def findProducts() {
    query = "select {product} from {processedproductjobitem} where {action} = 'GROOVY_MEDIA_CONVERSION_PRODUCTION' and {creationtime} > to_date('11-jul-2017 07:00:00', 'dd-mon-yyyy hh24:mi:ss') and {reason} = 'NOT_FOUND_MEDIAS|NO_MEDIAS|'"
    flexibleSearch = new FlexibleSearchQuery(query)
//    flexibleSearch.count = 2

    flexibleService.search(flexibleSearch).result
}

products = findProducts()
products.each {
    product = it
    reason = ""
    product.galleryImages.find {
        if (it == null) {
            reason = "NO_MEDIA"
            return true
        } else if (it.conversionStatus != ConversionStatus.CONVERTED) {
            reason = it.conversionStatus
            return true;
        }
        reason = "OK"

        return false
    }
    println product.code + "|" + product.name + "|" + product.approvalStatus + "|" + product.creationtime + "|" + reason
}
