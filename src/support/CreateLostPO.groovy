import com.vingroup.rocket.core.model.*
import de.hybris.platform.core.enums.OrderStatus
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.core.model.order.OrderEntryModel
import de.hybris.platform.core.model.user.AddressModel
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.storelocator.model.PointOfServiceModel

PoId = '706422314702'
MerchantCode = 'CHK'
ProductItemId ='PRI953928_CHK'
SellPrice = 164400
WhSapId = '1535'
Amount = 1
consignmentCode = 'cons7064223147_2'



vinMerchantOfferService = spring.getBean "vinMerchantOfferService"
vinMerchantVendorService = spring.getBean "vinMerchantVendorService"
modelService = spring.getBean "modelService"
vinWarehouseService = spring.getBean "vinWarehouseService"
flexibleSearchService = spring.getBean "flexibleSearchService"
vinShippingFeeConfigurationDao = spring.getBean "vinShippingFeeConfigurationDao"
vinPurchaseOrderDao = spring.getBean "vinPurchaseOrderDao"
vinSAPService = spring.getBean "vinSAPService"



product = vinMerchantOfferService.getOfferForCode(ProductItemId)
commissionContract =  vinMerchantVendorService.getEffectiveContractDetailForMerchantAndProduct(MerchantCode, product)
contractBusinessModel = commissionContract.merchantContract.contractBusinessModel
productType = product.product.productType
warehouse = vinWarehouseService.getWarehouse(MerchantCode, WhSapId)
merchant = product.merchant
consignment = findConsignmentByCode(consignmentCode)
so = consignment.order
deliveryArea = getDeliveryArea(so)
articleNumber = vinSAPService.findSAPArticleNumber(product, warehouse, commissionContract.merchantContract)
isVinIDRedeemAble = vinMerchantOfferService.isVinIDRedeemable(ProductItemId)


orderEntry = getOrderEntry(so, ProductItemId)

shippingFee = getShippingFee(warehouse, deliveryArea)
existingPO = vinPurchaseOrderDao.findByPOCodeAndOrder(PoId, so)


if (existingPO != null) {
    println " PO IS EXIST "
}
if (isNull(consignment, warehouse, shippingFee, orderEntry, articleNumber)) {
    println "CANNOT CREATE PO"
    return;
}





// Create PO
if (!existingPO) {
    foundShipment = findShipmentWithoutpo(so)
    if (foundShipment == null) return
    po = modelService.create(VinPurchaseOrderModel.class)
    po.code = PoId
    po.warehouse = warehouse
    po.productType = productType
    po.selfDelivery = product.getEffectiveSelfDelivery()
    po.merchant = merchant
    po.contractBusinessModel = contractBusinessModel
    po.consignment = consignment
    po.status = OrderStatus.PICKED
    po.shippingFee = shippingFee
    po.eligibleVinID = isVinIDRedeemAble
    po.shipment = foundShipment
} else {
    po = existingPO
}



// Create Consignment Entry
ConsignmentEntryModel consignmentEntry = modelService.create(ConsignmentEntryModel.class)
consignmentEntry.quantity = Amount
consignmentEntry.actualPickedQuantity = Amount
consignmentEntry.confirmedQuantity = Amount
consignmentEntry.finalPrice = SellPrice
consignmentEntry.status = OrderStatus.PICKED
consignmentEntry.purchaseOrder = po
consignmentEntry.consignment = consignment
consignmentEntry.orderEntry = orderEntry

itemEntries = []
// Create Item Entry
for (i = 0; i < Amount; i++) {
    ItemEntryModel item = new ItemEntryModel()
    item.consignmentEntry = consignmentEntry
    item.finalPrice = SellPrice
    item.baseOfferPrice = orderEntry.basePrice
    item.status = OrderStatus.PICKED
    item.sapArticleNumber = articleNumber
    itemEntries.add(item)

}

modelService.saveAll(itemEntries)
modelService.save(consignmentEntry)
if (!existingPO) {
    modelService.save(po)
}

return "DONE FOR PO " + PoId




def findConsignmentByCode(code) {
    query = "SELECT {PK} FROM {Consignment} where {code} = '" + code + "'"
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query)
    result = flexibleSearchService.search(flexibleSearchQuery).result

    result.each {
        consignment = it;
        order = consignment.order

        if (order != null ) {
            println consignment
            if (consignment.purchaseOrder != null) {
                foundConsignment = consignment
            }
        }
    }
    return foundConsignment
}

protected DeliveryAreaModel getDeliveryArea(final AbstractOrderModel abstractOrderModel)
{
    final AddressModel deliveryAddress = abstractOrderModel.getSourcingAddress();
    final DeliveryAreaModel deliveryArea = new DeliveryAreaModel();
    deliveryArea.setProvince(deliveryAddress.getProvince());
    deliveryArea.setDistrict(deliveryAddress.getVndistrict());
    deliveryArea.setWard(deliveryAddress.getWard());
    return deliveryArea;
}

private getShippingFee(warehouse, shippingArea) {
    final PointOfServiceModel pointOfService = warehouse.getPointsOfService().iterator().next();
    final ShippingFeeModel shippingFee = vinShippingFeeConfigurationDao
            .findShippingFeeByShippingAndPickingArea(pointOfService.getAddress(), shippingArea);
    return shippingFee;
}

private OrderEntryModel getOrderEntry(order, productCode) {
    order.entries.find {
        if (it.product.code.equals(productCode)) {
            foundOrderEntry = it
            return true
        }
        return false
    }
}

private boolean isNull(Object... objects) {
    isNull = false;
    println objects
    objects.find {
        isNull = (it == null)
        return isNull
    }

    return isNull;

}


def findShipmentWithoutpo(so) {
    foundshipment = null
    so.shipmentEntries.find{
        shipment = it
        if (shipment.purchaseOrder == null || shipment.purchaseOrder.size() == 0) {
            foundshipment = it
            return true
        }
        return false
    }

    return foundshipment
}
