import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

/**
 * Created by Khoi Nguyen on 6/23/2017.
 */
flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
categoryService = spring.getBean "categoryService"



def findRPO() {
    def queryStr = "select {pk} from  {VinReturnPurchaseOrder}" ;

    def query = new FlexibleSearchQuery(queryStr)

    println query;
    flexibleSearchService.search(query).result
}

rpos = findRPO()
listRpos = []
rpos.each {
    rpo = it
    it.getReturnPurchaseOrderEntries().each{
        offer = it.getItemEntry().getConsignmentEntry().getOrderEntry().getProduct()
        if (offer == null) {
            listRpos.add(rpo)
        }

    }
}

myset = listRpos.toSet()
myset.each {
    modelService.remove(it)
}
