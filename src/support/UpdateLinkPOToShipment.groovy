package support

/**
 * Created by Khoi Nguyen on 7/2/2017.
 */


vinMerchantOfferService = spring.getBean "vinMerchantOfferService"
vinMerchantVendorService = spring.getBean "vinMerchantVendorService"
modelService = spring.getBean "modelService"
vinWarehouseService = spring.getBean "vinWarehouseService"
flexibleSearchService = spring.getBean "flexibleSearchService"
vinShippingFeeConfigurationDao = spring.getBean "vinShippingFeeConfigurationDao"
vinPurchaseOrderDao = spring.getBean "vinPurchaseOrderDao"
vinSAPService = spring.getBean "vinSAPService"

PoId = ["706422314701", "706422314702"]


po1 = vinPurchaseOrderDao.findByCode(PoId.get(0))
shipment = findShipmentWithoutpo(po1)


if (shipment == null) {
    return "NOT FOUND SHIPMENT "
}

PoId.each {
    po = vinPurchaseOrderDao.findByCode(it)
    println po.consignment.order
    po.shipment = shipment
    modelService.save(po)

}

return "DONE"

def findShipmentWithoutpo(po) {
    foundshipment = null
    po.consignment.order.shipmentEntries.find{
        shipment = it
        if (shipment.purchaseOrder == null || shipment.purchaseOrder.size() == 0) {
            foundshipment = it
            return true
        }
        return false
    }

    return foundshipment
}
