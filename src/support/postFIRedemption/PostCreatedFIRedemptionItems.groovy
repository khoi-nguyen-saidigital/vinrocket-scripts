import com.vingroup.rocket.core.model.ItemEntryModel
import de.hybris.platform.util.CSVReader

mediaName = "sap_redemption_1211.csv"
media = mediaService.getMedia(mediaName)
content = mediaService.getDataFromMedia(media)
String s = new String(content)

CSVReader reader = new CSVReader(s)
Map<String, List<Integer>> orderMap = new HashMap<>()

while (reader.readNextLine()) {
    line = reader.line
    orderCode = line.get(0)
    itemNumber = Integer.parseInt(line.get(1))


    List<Integer> listItem = new ArrayList<>()
    if (orderMap.containsKey(orderCode)) {
        listItem = orderMap.get(orderCode)
    }

    listItem.add(itemNumber)
    orderMap.put(orderCode, listItem)
}

orderMap.each {
    orderCode = it.key
    itemNumbers = it.value
    order = vinOrderService.getSaleOrderModelByCode(orderCode)
    List<ItemEntryModel> processingItems = new ArrayList<>()
    order.itemEntries.each {
        item = it;
        if (itemNumbers.contains(item.itemNumber)) {
            processingItems.add(item)
            println orderCode + "|" + item.itemNumber + "|" + item.status + "|" + item.serialStatus.code
        }
    }
//    sapPostingService.postVoucherRedemAtMC(processingItems)
}