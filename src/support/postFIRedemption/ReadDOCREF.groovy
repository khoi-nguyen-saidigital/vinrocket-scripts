import org.apache.commons.io.FileUtils
import vincm.bris.bris.to.sapecc.commission.DTBRISCOMMISSION
import vincm.bris.bris.to.sapecc.commission.ObjectFactory

import javax.xml.bind.JAXBContext
import javax.xml.bind.JAXBElement
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAttribute
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement


dir = "/opt/vinrocket/hybris/hybris/data/idocs/outbound/fi/"
allFiles = new File(dir).listFiles()
redemptionFiles = [];
allFiles.each {
    if (it.name.contains("PRD_BRIS_MKTPLACE_ACCRUAL")) {
        redemptionFiles.add(it)
    }
}
println redemptionFiles.size()
println "REFDOC;ORDER;ALLOC_NUMBER;FILE_NAME"
redemptionFiles.each {
    readContent(it)
}

def readContent(file) {
//    file = new File(filePath)
    fileName = file.name
    content = FileUtils.readFileToString(file)
    xml = new XmlSlurper().parseText(content)
    ref = xml.Header.REF_DOC_NO
    vendors = xml.VendorItems
    vendors.each {
        output = "" + xml.Header.REF_DOC_NO + ";" + it.REF_KEY_1 + ";" + it.ALLOC_NMBR + ";" + fileName
        println output
    }
}
