import com.vingroup.rocket.core.enums.SerialStatus
import com.vingroup.rocket.core.model.ItemEntryModel
import de.hybris.platform.core.enums.OrderStatus
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import org.apache.commons.collections.CollectionUtils

Set<String> orderCodes = new HashSet<>()
Map<String, List<ItemEntryModel>> orderMap = new HashMap<>()

findCreatedProcess().each {
    code = it.code.replace("booking-serials-redemption-process-", "")
    code = code.split("-")[0]
    orderCodes.add(code)
}

orderCodes.each {
    orderCode = it
    order = vinOrderService.getSaleOrderModelByCode(orderCode)
    List<ItemEntryModel> oriItems = new ArrayList<>();
    order.itemEntries.each {
        item = it
        if (item.serialStatus != null && item.serialStatus.code.equals(SerialStatus.USED) && item.status.equals(OrderStatus.CLOSED)) {
            oriItems.add(item)
        }
    }
    business = findBusiness(orderCode)

    if (CollectionUtils.isNotEmpty(business)) {

        business.each {
            items = it.itemEntries
            oriItems = CollectionUtils.subtract(oriItems, items)
        }
    }

    orderMap.put(orderCode, oriItems)
}

orderMap.each {
    code = it.key
    it.value.each {
        output = code + "|" + it.serialNumber + "|" + it.itemNumber
        println output
    }

}
def findCreatedProcess() {
    FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {PK} FROM {BookingSerialsRedemptionProcess as b JOIN EnumerationValue as state ON {b.state} = {state.pk}} WHERE {state.code} = ?state")
    query.addQueryParameter("state", "CREATED")
    flexibleSearchService.search(query).result
}


def findBusiness(orderCode) {
    FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {PK} FROM {BookingSerialsRedemptionProcess as b JOIN Order as o ON {b.order} = {o.PK}} WHERE {o.code} = ?code")
    query.addQueryParameter("code", orderCode)
    flexibleSearchService.search(query).result
}