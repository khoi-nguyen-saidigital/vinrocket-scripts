import org.apache.commons.io.FileUtils

import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes

String backupFolder = "/backup/idocs/outbound/"
List<File> files = new ArrayList<>()

new File(backupFolder).listFiles().each {
    absolutePath = it.absolutePath + "/outbound/fi/"
    new File(absolutePath).listFiles().each{
        name = it.name
        if (name.contains("PRD_BRIS_MKTPLACE_ACCRUAL")) {
            files.add(it)
        }
    }
}

new File("/opt/vinrocket/hybris/hybris/data/idocs/outbound/fi/").listFiles().each {
    name = it.name
    if (name.contains("PRD_BRIS_MKTPLACE_ACCRUAL")) {
        files.add(it)
    }
}

files.each {
    name = it.name.replace("PRD_BRIS_MKTPLACE_ACCRUAL_", "")
    BasicFileAttributes attr = Files.readAttributes(it.toPath(), BasicFileAttributes.class)
    creationTime = new Date(attr.creationTime().toMillis())
    println name.replace("_", "|") + "|" + creationTime + "|" + it
}

return ""