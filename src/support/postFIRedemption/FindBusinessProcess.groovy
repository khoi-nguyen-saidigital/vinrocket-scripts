import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.util.CSVReader
import org.apache.commons.collections.CollectionUtils

mediaName = "sap_redemption_1.csv"
MSG_TYPE = MessageType.NOT_HAD_BUSINESS
media = mediaService.getMedia(mediaName)

content = mediaService.getDataFromMedia(media)
String s = new String(content)

CSVReader reader = new CSVReader(s)
Map<String, List<Integer>> orderMap = new HashMap<>()
List<String> serials = new ArrayList<>()
List<String> outputList = new ArrayList<>()

while (reader.readNextLine()) {
    line = reader.line
    orderCode = line.get(0)
    itemNumber = Integer.parseInt(line.get(1))
    serialNumber = line.get(2)

    Item i = new Item()
    i.itemNumber = itemNumber
    i.serialNumber = serialNumber

    List<Integer> listItem = new ArrayList<>()
    if (orderMap.containsKey(orderCode)) {
        listItem = orderMap.get(orderCode)
    }

    listItem.add(itemNumber)
    orderMap.put(orderCode, listItem)
    serials.add(serialNumber)
}

orderMap.each {
    orderCode = it.key
    businesses = findBusiness(orderCode)
    if (CollectionUtils.isNotEmpty(businesses)) {
        hasError = false
        errorOrderCode = ""
        clusterId = ""
        items = it.value
        findBusiness(orderCode).each {
            business = it
            it.itemEntries.each {
                if (items.contains(it.itemNumber)) {
                    output = orderCode + "|" + it.serialNumber + "|" + it.itemNumber + "|" + business.taskLogs.get(0).clusterId + "|" + business.code + "|" + business.state  + "|" + business.creationtime
                    println(output, MessageType.HAD_BUSINESS_PROCESS)
                }
            }
        }
    }

    if (MSG_TYPE == MessageType.NOT_HAD_BUSINESS) {
        createdBusiness = findCreatedBusinessProcess(orderCode)
        if (CollectionUtils.isNotEmpty(createdBusiness)) {
            createdBusiness.each {
                println orderCode + "|" + it.code

            }
        } else {
//            println orderCode
        }

    }
}

def println(String msg, MessageType type) {
    if (type.equals(MSG_TYPE)) {
        println msg
    }
}

def findCreatedBusinessProcess(orderCode) {
    FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {PK} FROM {BookingSerialsRedemptionProcess as b JOIN EnumerationValue as state ON {b.state} = {state.pk}} WHERE {b.code} like ?code AND {state.code} = ?state")
    query.addQueryParameter("code", "booking-serials-redemption-process-" + orderCode + "%")
    query.addQueryParameter("state", "CREATED")
    flexibleSearchService.search(query).result
}

def findBusiness(orderCode) {
    FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {PK} FROM {BookingSerialsRedemptionProcess as b JOIN Order as o ON {b.order} = {o.PK}} WHERE {o.code} = ?code")
    query.addQueryParameter("code", orderCode)
    flexibleSearchService.search(query).result
}

class Order {
    String orderCode;
    List<Item> items;
}
class Item {
    String serialNumber;
    String itemNumber;
}

enum MessageType {
    HAD_BUSINESS_PROCESS,
    NOT_HAD_BUSINESS
}