package support

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

/**
 * Created by Khoi Nguyen on 7/10/2017.
 */
poId = "707903216001"
vinPurchaseOrderService = spring.getBean "vinPurchaseOrderService"
flexibleSearchService = spring.getBean "flexibleSearchService"

findPosByCode(poId).each {
    po = it
    println po
    po.consignment.each {
        consignment = it

        println consignment.order
    }
}

def findPosByCode(code) {
    FlexibleSearchQuery query = new FlexibleSearchQuery("select {pk} from {VinPurchaseOrder} where {code} = ?code")
    query.addQueryParameter("code", code)
    return flexibleSearchService.search(query).result
}
