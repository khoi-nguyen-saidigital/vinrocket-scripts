package support

/**
 * Created by Khoi Nguyen on 7/3/2017.
 */

vinMerchantVendorService = spring.getBean "vinMerchantVendorService"
vinMerchantOfferService = spring.getBean "vinMerchantOfferService"

merchantCode = "LZZ"
productCode = "PRI924399_LZZ"
product = vinMerchantOfferService.getOnlineOfferWithNoSearchRestriction(productCode)
println product.code

isVinIDRedeemable = vinMerchantOfferService.isVinIDRedeemable(product)
println isVinIDRedeemable

contractDetail = vinMerchantVendorService.getEffectiveContractDetailForMerchantAndProduct(merchantCode, product)
merchantContract = contractDetail.merchantContract
contractBusinessModel = merchantContract.contractBusinessModel
contractBusinessModel.code
