package support

import com.vingroup.rocket.core.enums.MerchantContractBusinessModel
import com.vingroup.rocket.core.enums.MerchantContractType
import com.vingroup.rocket.core.enums.RegconitionType
import com.vingroup.rocket.core.enums.RevenueRecognitionAt
import com.vingroup.rocket.core.enums.SalesType
import com.vingroup.rocket.core.model.MerchantContractModel
import com.vingroup.rocket.core.model.MerchantVendorModel
import com.vingroup.rocket.posdm.common.POSDMUtils
import de.hybris.platform.ordersplitting.model.WarehouseModel

/**
 * Created by Khoi Nguyen on 6/26/2017.
 */
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

/**
 * Created by Khoi Nguyen on 6/23/2017.
 */
flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
categoryService = spring.getBean "categoryService"
vinSAPService = spring.getBean "vinSAPService"
vinWarehouseService = spring.getBean "vinWarehouseService"


def findOffer(offerCode) {
    def queryStr = "select {pk} from  {MerchantOffer} where {code} = '" + offerCode + "' ";


    def query = new FlexibleSearchQuery(queryStr)

    println query;
    flexibleSearchService.search(query).result
}

offerCode = 'duyendeal_CUK'
warehouseStoreCode = 'WH1288'
offer = findOffer(offerCode).get(0);
offerModel = offer

warehouse = vinWarehouseService.getWarehouseByStoreCode(warehouseStoreCode)
merchant = offer.merchant;
contracts = merchant.getMerchantContracts();
dummyProducts = offer.getProduct().getCommissionCategory().getDummyProducts()
contracts.each {
    contract = it
    merchantContractType = contract.getContractBusinessModel();

    if (POSDMUtils.isRetail(offerModel.getMerchant(), warehouse, contract.getContractBusinessModel())) {

    }

    dummyProducts.each {
        dummy = it
        if (contract != null && contract.getContractBusinessModel() != null
                && matchDummyWithContractForSales(dummy.getSalesType(), contract.getContractBusinessModel())
                && matchDummyWithContractForReconition(dummy.getRegconitionType(), contract))
        {
            println 'match'
        } else {
            println 'not match'
        }
    }
}


private boolean matchDummyWithContractForSales(final SalesType salestype, final MerchantContractBusinessModel businessModel)
{
    if (salestype.equals(SalesType.TRADING) && businessModel.equals(MerchantContractBusinessModel.TRADING))
    {
        return true;
    }
    else if (salestype.equals(SalesType.COMMISSION) && businessModel.equals(MerchantContractBusinessModel.COMMISSION))
    {
        return true;
    }
    else if (salestype.equals(SalesType.TRADING) && businessModel.equals(MerchantContractBusinessModel.COMMISSIONWITHINVOICE))
    {
        return true;
    }
    return false;
}

private boolean matchDummyWithContractForReconition(final RegconitionType recotype, final MerchantContractModel contract)
{
    if (!contract.getMerchantContractType().equals(MerchantContractType.NORMAL))
    {
        final RevenueRecognitionAt recoAt = contract.getRevenueRecognitionAt();

        if (recotype.equals(RegconitionType.COMMISSIONATBOOKING) && recoAt.equals(RevenueRecognitionAt.BOOKING))
        {
            return true;
        }
        else if (recotype.equals(RegconitionType.COMMISSIONATREDEMPTION) && recoAt.equals(RevenueRecognitionAt.REDEMPTION))
        {
            return true;
        }
        else if (recotype.equals(RegconitionType.COMMISSIONATSALES) && recoAt.equals(RevenueRecognitionAt.SALES))
        {
            return true;
        }
        return false;
    }
    return true;
}
