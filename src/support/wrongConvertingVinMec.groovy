import com.vingroup.rocket.core.model.DisplayCategoryModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.variants.model.GenericVariantProductModel

String q = "SELECT {process.product} FROM {ProcessedProductJobItem as process} where {process.product} IN ({{ select {product} FROM  {ProcessedProductJobItem}  WHERE {action} = 'GROOVY_SET_MEDIA_CONVERSION_GROUP_PRODUCTION' and {reason} = 'NORMAL'  and {creationtime} between to_date('11/12/2017 12:00:00', 'dd/mm/yyyy hh24:mi:ss') AND to_date('11/12/2017 17:20:00', 'dd/mm/yyyy hh24:mi:ss')                                                                                                                         }})  AND {process.reason} != 'CONVERTED_ALREADY' AND {process.action} = 'GROOVY_CONVERT_MEDIA_PRODUCTION_ROUND_3'"
FlexibleSearchQuery query = new FlexibleSearchQuery(q)
flexibleSearchService.search(query).result.each {
    product = it
    baseProduct = it
    if (it instanceof GenericVariantProductModel) {
        baseProduct = it.baseProduct
    }
    baseProduct.supercategories.each {
        if (it instanceof DisplayCategoryModel) {
            println "" + product.code + "|" + it.code + "|" + it.name
        }

    }
}

