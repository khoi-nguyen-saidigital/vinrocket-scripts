package support

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import groovy.json.JsonSlurper
import org.apache.commons.lang3.StringUtils

flexibleSearchService = spring.getBean "flexibleSearchService"
vinMerchantOfferService = spring.getBean "vinMerchantOfferService"

minStock = 50
/**
 * Created by Khoi Nguyen on 7/6/2017.
 */

findPromotions().each {
    promotion = it
    action = promotion.actions
    if (action.contains("vin_free_gift")) {
        println "================"
        println promotion.code
        println promotion.conditions
        println promotion.actions
        println "================"
    }
//    jsonSlurper = new JsonSlurper()
//    object = jsonSlurper.parseText(conditions)
//    offerCode = findProductCondition(object)
//    if (offerCode != null) {
//        offer = vinMerchantOfferService.getOfferForCode(offerCode)
//        stock = vinMerchantOfferService.getAvailableStockForOffer(offer)
//        if (stock > minStock) {
//            println promotion.code + " :::: " + offerCode
//        }
//    }
}

def findActions(actions) {

}
def findProductCondition(conditions) {

    product = ""
    merchant = ""
    conditions.each {
        if (it.definitionId == 'vin_qualifying_products') {
            product = it.parameters.products.value[0]
        } else if (it.definitionId == 'merchant_vendor') {
            merchant += it.parameters.merchant_vendor.value[0]
        }
    }

    if (StringUtils.isNotEmpty(merchant)) {
        merchant = "_" + merchant
        return product + merchant
    }
    return null;

}

def findPromotions() {
    promotionQuery = "SELECT {pk} FROM {PromotionSourceRule as p JOIN EnumerationValue as s ON {p.status} = {s.pk}   } WHERE {s.code} = 'PUBLISHED' order by {creationtime} desc"
    FlexibleSearchQuery query = new FlexibleSearchQuery(promotionQuery)
//    query.count = 500
    flexibleSearchService.search(query).result
}
