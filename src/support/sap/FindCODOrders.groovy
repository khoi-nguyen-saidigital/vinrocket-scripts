import de.hybris.platform.core.model.order.OrderModel
napasPayment = flexibleSearchService.search("SELECT {PK} FROM {PaymentMode} WHERE {CODE} = 'NAPAS'").result.get(0)

orderCodes = ['7119273061']
orderCodes.each {
    order = vinOrderService.getSaleOrderModelByCode(it)
    if (isWrongOrder(order)) {
        println order.code
        correctPaymentMethod(order)
    } else {

        println order.code + " is not wrong"
    }


}

def correctPaymentMethod(OrderModel order) {
    order.setPaymentMode(napasPayment)
}

def isWrongOrder(OrderModel orderModel) {
    boolean ignore = false;
    order.paymentTransactions.each {
        if (it.paymentProvider.equals("vinIDRedeem")) {
            ignore = true;
        }
    }
    return !ignore;
}
def findWrongCODOrder() {
    allPendingOrders = []
    findCODOrdersWithoutVINIDPaymentTransaction().each {
        boolean ignore = false;
        OrderModel order = it
        order.paymentTransactions.each {
            if (it.paymentProvider.equals("vinIDRedeem")) {
                ignore = true;
            }
        }
        if (!ignore) {
            allPendingOrders.add(it)
        }

        allPendingOrders.each {
            println it.code
        }
    }
    return allPendingOrders
}


def findCODOrdersWithoutVINIDPaymentTransaction() {
    query = "select distinct{o.pk} from {Order as o JOIN PaymentMode as p ON {p.pk} = {o.paymentmode} JOIN PaymentTransaction as pt ON {pt.order} = {o.pk}} \n" +
            "WHERE {p.code} = 'COD' AND {pt.pk} is not null AND {pt.paymentProvider} != 'vinIDRedeem'"

    return flexibleSearchService.search(query).result
}
