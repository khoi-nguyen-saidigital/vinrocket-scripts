duplicatedItems = flexibleSearchService.search("select {PK} from {ItemSyncTimeStamp} where {sourceItem} = {targetItem}").result
log("THERE ARE " + duplicatedItems.size() + " ITEMS")

modelService.removeAll(duplicatedItems)

def log(msg) {
//    println msg
    log.error(msg)
}