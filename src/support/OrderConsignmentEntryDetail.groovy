package support

/**
 * Created by Khoi Nguyen on 7/1/2017.
 */

def orderCode = "7105056100-temp"


orderService = spring.getBean "vinOrderService"
flexibleSearchService = spring.getBean "flexibleSearchService"
order = flexibleSearchService.search("select {pk} from {Cart} where {code} = '7105056100-temp'").result.get(0)
println "Order is created at " + order.creationtime
println "Order status is " + order.status
order.consignmentEntries.size()