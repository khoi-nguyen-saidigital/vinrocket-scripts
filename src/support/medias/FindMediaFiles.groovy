import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

flexibleSearchService = spring.getBean "flexibleSearchService"

ACTION = "GROOVY_SET_MEDIA_CONVERSION_GROUP_PRODUCTION"
PRODUCT_TYPE = "DEAL"

findProducts().each {
    product = it.product
    if (product != null) {
        productCode = product.code

        product.galleryImages.each {
            mediaList = findMediaByContainer(it)
            mediaList.each {
                output = productCode + "|" + it.location + "|" + it.URL + "|" + it.size
                println output
            }
        }

    }
}

def findProducts() {
    query = "SELECT {PK} FROM {ProcessedproductJobItem} where {action} = ?action and {reason} = ?productType"
    FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query)

    searchQuery.addQueryParameter("action", ACTION)
    searchQuery.addQueryParameter("productType", PRODUCT_TYPE)

    searchQuery.count = 10
    flexibleSearchService.search(searchQuery).result
}

def findMediaByContainer(container) {
    query = "SELECT {PK} FROM {ProductMedia} where {mediaContainer} = ?container"
    FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query)

    searchQuery.addQueryParameter("container", container)

    flexibleSearchService.search(searchQuery).result
}