

/**
 * Created by Khoi Nguyen on 6/22/2017.
 */
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import com.vingroup.rocket.core.model.*;

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"

stagedProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Staged")


def find() {
    query = new FlexibleSearchQuery("select {product} from {ProcessedProductJobItem} where {action} = 'FILL_GALLERY_IMAGES' and {product} not in ({{select {product} from {ProcessedProductJobItem} where {action} = 'GROOVY_CLEANUP'}})")

    println query;
    query.count = 20000
    flexibleSearchService.search(query).result
}


result  = find()
result.each {
    productCode = it.code
    product = it;
    stagedProduct = productService.getProductForCode(stagedProductCatalog, productCode)

    boolean needToRemove = false;
    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel();
    item.product = it;
    item.productCode = productCode;
    item.action = 'GROOVY_CLEANUP'

    galleryImages = it.galleryImages.collect();
    i = galleryImages.iterator()
    while (i.hasNext()) {
        gallery = i.next();
        qualifier = gallery.qualifier;
        if (qualifier.equals(productCode) || qualifier.startsWith(productCode + "_")) {

        } else {
            i.remove();
            needToRemove = true;
        }
    }
    item.reason = "NO_CHANGE"

    log.info("PROCESSING CLEANUP FOR PRODUCT " + productCode)
    if (needToRemove) {
        try {
            reason = product.galleryImages.size() + "==>" + galleryImages.size()
            log.info("NEED TO CLEANUP FOR PRODUCT" + productCode + " reason [" + reason + "]")

            println productCode
            println reason
            item.reason = reason
            product.galleryImages = galleryImages
            stagedProduct.galleryImages = galleryImages

            modelService.save(product)
            modelService.save(stagedProduct)
        } catch (Exception e) {
            println e.getMessage()
            item.reason = "ERROR: " + e.getMessage()
        }
    }

    modelService.save(item)
}
