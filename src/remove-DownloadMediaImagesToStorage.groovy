import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"


/*
*
* adayroiLegacyProductMediaResource ==> file:/storage/do_not_delete_ever
*
* */
def find() {
//    def conversiongroup = "8796093120913" //fashion
    def conversiongroup = "8796125823377" //ict

//    def conversiongroup = "8796093055377" //fresh
//    def conversiongroup = "8796093088145" //deal


    def contentCatalogVersion = 8796093153881 // Product Catalog Stage
//    def contentCatalogVersion = 8796093186649 // Product Catalog Online

    query = new FlexibleSearchQuery("select {pk} from " +
            "{MediaContainer as mc JOIN ProductMedia as pm ON {pm.mediaContainer} = {mc.pk} }" +
            " WHERE {mc.conversiongroup} = " + conversiongroup + " AND {pm.internalURL} <> 'replicated273654712'" +
            " AND {pm.catalogVersion} = " + contentCatalogVersion)
    flexibleSearchService.search(query).result
}

result = find()
result.each {
    medias = it.getMedias();
    medias.each {

        url = it.getURL()
        newUrl = url.replaceAll("//cdn02.static-adayroi.com", '\\$config-adayroiLegacyProductMediaResource')
        println ";" + it.code + ";;;" + newUrl
    }
}

