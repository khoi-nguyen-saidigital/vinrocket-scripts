import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableSet
import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.servicelayer.interceptor.impl.InterceptorExecutionPolicy
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.session.SessionExecutionBody
import org.apache.commons.collections.CollectionUtils

/**
 * Created by Khoi Nguyen on 6/22/2017.
 */

/*
*
* select * from {ProcessedProductJobItem} where {action} = 'GROOVY_FILLING_GALLERY_IMAGE' and {reason} not in ('ERROR', 'IGNORE', 'NOT_FOUND_MEDIAS')
*
* */

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
sessionService = spring.getBean "sessionService"

stagedProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Staged")

CATALOG_VERSION = stagedProductCatalog.pk
ACTION = 'GROOVY_FILLING_GALLERY_IMAGE_PRODUCTION'

def findProducts() {
    queryStr = " SELECT distinct{P.pk} FROM {Product AS P JOIN MerchantOffer AS MO ON {MO.product} = {P.pk} JOIN MerchantVendor AS M ON {MO.merchant} = {M.pk} JOIN EnumerationValue AS MS ON {M.status} = {MS.pk} JOIN EnumerationValue AS PS ON {P.approvalStatus} = {PS.pk}} WHERE {PS.code}='approved' AND {P.pk} NOT IN ({{ SELECT {product} FROM {ProcessedProductJobItem} WHERE {action} = '" + ACTION + "' AND {reason} != 'NOT_FOUND_MEDIAS'}}) AND {P.variantType} is null AND {P.catalogVersion} = " + CATALOG_VERSION + " ORDER BY {P.PK}"

    query = new FlexibleSearchQuery(queryStr)

    println query;
    query.count = 100000
    flexibleSearchService.search(query).result
}

def findMediaByCode(code) {
    primaryImage = findExtractMediaByCode(code)
    if (primaryImage != null) {
        relevantMedias = new ArrayList(findRelevantMediasByCode(code))
        relevantMedias.add(0, primaryImage)
        return relevantMedias
    }
}

def findExtractMediaByCode(code) {
    queryStr = "SELECT {PK} FROM {MediaContainer} WHERE {qualifier} = '" + code + "' AND {catalogversion} = " + CATALOG_VERSION
    query = new FlexibleSearchQuery(queryStr)
    result = flexibleSearchService.search(query)
    if (result.getResult().size() > 0) {
        return result.getResult().get(0)
    }
}

def findRelevantMediasByCode(code) {
    queryStr = "SELECT {PK} FROM {MediaContainer} WHERE {qualifier} like '" + code + "\\_%' escape '\\' AND {catalogversion} = " + CATALOG_VERSION
    query = new FlexibleSearchQuery(queryStr)
    result = flexibleSearchService.search(query)
    return result.getResult()
}

def log(message) {
    log.info(message)
}

products = findProducts()
products.each {
    product = it;
    productCode = product.code
    log("FILLING GROOVY MEDIA FOR " + productCode)

    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel()
    item.setProduct(product)
    item.setProductCode(productCode)
    item.setAction(ACTION)

    try {
        reason = "";
        originalMedias = product.galleryImages;
        if (originalMedias != null && originalMedias.size() > 0) {
            reason = "PRODUCT_HAS_GALLERY"
        } else {
            reason = "NOT_FOUND_MEDIAS"
        }

    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason = "ERROR | " + errorMsg
        log(e)
    }


    message = "FINISH GROOVY FILLING MEDIA FOR product [" + productCode + "] with reason [" + reason + "]"
    log(message )
    item.setReason(reason)
    modelService.save(item)

}

private void saveProductInLocalContext(product) {
    Map<String, Object> params = ImmutableMap.of(InterceptorExecutionPolicy.DISABLED_INTERCEPTOR_TYPES,
            ImmutableSet.of(InterceptorExecutionPolicy.InterceptorType.VALIDATE));
    sessionService.executeInLocalViewWithParams(params, new SessionExecutionBody()
    {
        @Override
        public void executeWithoutResult()
        {
            // Persist changes to DB
            modelService.save(product);
        }
    });
}

