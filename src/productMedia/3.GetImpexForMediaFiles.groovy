package productMedia

import com.vingroup.rocket.core.model.DisplayCategoryModel
import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.media.MediaModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.mediaconversion.model.ConversionGroupModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.variants.model.VariantProductModel
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.ListUtils
import org.apache.commons.lang.StringUtils

/**
 * Created by Khoi Nguyen on 6/22/2017.
 */

/*
*
* select count(*) from {ProcessedProductJobItem} where {action} = 'GROOVY_SET_MEDIA_CONVERSION_GROUP' and {reason} not in ('ALREADY_SET', 'NO_MEDIAS')
*
* */

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
categoryService = spring.getBean "categoryService"
productCategoryTypeMap = new HashMap<>()
conversionGroupModelMap = new HashMap<>()


stagedProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Staged")

productCatalogVersionOnline = 8796093186649
productCatalogVersionStaged = 8796093153881
CATALOG_VERSION = productCatalogVersionStaged
ACTION = 'GROOVY_SET_MEDIA_CONVERSION_GROUP'

def findProducts() {
    def queryStr = "select {product} from {ProcessedProductJobItem} where {action} = 'GROOVY_SET_MEDIA_CONVERSION_GROUP'" ;
    queryStr = "select distinct{product} from {ProcessedProductJobItem} where {action} = 'GROOVY_CONVERT_MEDIA' and {reason} = 'ERROR | null'"

    def query = new FlexibleSearchQuery(queryStr)

    println query;
//    query.count = 2000
    flexibleSearchService.search(query).result
}

def log(message) {
    //println message;
//    log.info(message)
}

products = findProducts()

reviewProducts = []

products.each {
    def product = it;
    def productCode = product.code
    log("SET MEDIA CONVERSION GROUP " + productCode)
    reason = "";
    galleryImages = product.galleryImages
    galleryImages.each {

        if (it.conversionGroup != null) {

            medias = it.medias
            medias.each {

                if (it.internalURL != 'replicated273654712') {
                    url = it.getURL()
                    newUrl = url.replaceAll("//cdn02.static-adayroi.com", 'file:///storage/do_not_delete_ever')
                    println ";" + it.code + ";;;" + newUrl
                }
            }
        }
    }
}
