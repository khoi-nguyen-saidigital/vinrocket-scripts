import com.vingroup.rocket.core.model.DisplayCategoryModel
import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.media.MediaModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.mediaconversion.model.ConversionGroupModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.variants.model.VariantProductModel
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.ListUtils
import org.apache.commons.lang.StringUtils

/**
 * Created by Khoi Nguyen on 6/22/2017.
 */

/*
*
* select count(*) from {ProcessedProductJobItem} where {action} = 'GROOVY_SET_MEDIA_CONVERSION_GROUP' and {reason} not in ('ALREADY_SET', 'NO_MEDIAS')
*
* */

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
categoryService = spring.getBean "categoryService"
productCategoryTypeMap = new HashMap<>()
conversionGroupModelMap = new HashMap<>()


PREVIOUS_ACTION = 'GROOVY_FILLING_GALLERY_IMAGE_PRODUCTION'
ACTION = 'GROOVY_SET_MEDIA_CONVERSION_GROUP_PRODUCTION'

def findProducts() {
    def queryStr = "SELECT distinct{product} FROM {ProcessedProductJobItem} where {action} = '" + PREVIOUS_ACTION + "' AND {reason} <> 'NOT_FOUND_MEDIAS' and {reason} not like 'ERROR %' AND {product} not in " +
            "({{ SELECT {product} FROM {ProcessedProductJobItem} WHERE {action} = '" + ACTION + "'}})"
    def query = new FlexibleSearchQuery(queryStr)

    println query;
    query.count = 10000
    flexibleSearchService.search(query).result
}

def log(message) {
    //println message;
    log.info(message)
}

initializeConfiguration()

products = findProducts()
products.each {
    def product = it;
    def productCode = product.code
    log("SET MEDIA CONVERSION GROUP " + productCode)

    reason = "";

    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel()
    item.setProduct(product)
    item.setProductCode(productCode)
    item.setAction(ACTION)

    try {
        List<MediaContainerModel> galleryImages = product.getGalleryImages();
        if (CollectionUtils.isEmpty(galleryImages)) {
            reason = "NO_MEDIAS"
        } else {
            ProductCategoryType productType = getCategoryTypeFromProduct(product);
            ConversionGroupModel conversionGroup = conversionGroupModelMap.get(productType);
            reason = productType.name()
            for (MediaContainerModel mediaContainer : galleryImages) {
                mediaContainer.setConversionGroup(conversionGroup);
                modelService.save(mediaContainer);
            }
        }

    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason = "ERROR | " + errorMsg
        log(e)
    }


    message = "FINISH SET MEDIA CONVERSION GROUP for product [" + productCode + "] with reason [" + reason + "]"
    log(message)
    item.setReason(reason)
    modelService.save(item)

}


private ProductCategoryType getCategoryTypeFromProduct(ProductModel productModel) {
    Collection<CategoryModel> categories = CollectionUtils.EMPTY_COLLECTION;
    if (productModel instanceof VariantProductModel) {
        categories = ((VariantProductModel) productModel).getBaseProduct().getSupercategories();
    } else if (productModel instanceof ProductModel) {
        categories = productModel.getSupercategories();
    }

    if (CollectionUtils.isNotEmpty(categories)) {
        for (CategoryModel displayCategory : categories) {
            if (displayCategory != null && displayCategory instanceof DisplayCategoryModel) {
                if (getCategoryTypeFromCategory(displayCategory) != null) {
                    //log("Product [" + productModel.getCode() + "] belongs to category [" + displayCategory.getCode() + "]");
                    return getCategoryTypeFromCategory(displayCategory);
                }
                Collection<CategoryModel> superCategories = categoryService.getAllSupercategoriesForCategory(displayCategory);
                if (CollectionUtils.isNotEmpty(superCategories)) {
                    for (CategoryModel category : superCategories) {
                        if (getCategoryTypeFromCategory(category) != null) {
                            //log("Product [" + productModel.getCode() + "] belongs to category [" + category.getCode() + "]");
                            return getCategoryTypeFromCategory(category);
                        }
                    }
                }

            }
        }
    }

    return ProductCategoryType.NORMAL;
}

private ProductCategoryType getCategoryTypeFromCategory(CategoryModel category) {
    if (category != null && category instanceof DisplayCategoryModel) {
        return productCategoryTypeMap.get(category.getCode());
    }

    return null;
}

private void initProductCategoryType(String categoryStr, ProductCategoryType type) {
    if (StringUtils.isNotEmpty(categoryStr)) {
        String[] categories = categoryStr.split(",");
        for (String categoryCode : categories) {
            productCategoryTypeMap.put(categoryCode, type);
        }
        log("Init for product category type: " + type + "[" + categoryStr + "]");
    }
}

private initializeConfiguration() {
    String fashionCategories = "1,780,842"
    initProductCategoryType(fashionCategories, ProductCategoryType.FASHION);
    String ictCategories = "321"
    initProductCategoryType(ictCategories, ProductCategoryType.ICT);
    String dealCategories = "332552,332555,332550,332551,3325581,332500,3325581"
    initProductCategoryType(dealCategories, ProductCategoryType.DEAL);

    List<ConversionGroupModel> conversionGroups = getConversionGroup()
    for (ConversionGroupModel conversionGroup : conversionGroups) {
        switch (conversionGroup.getCode()) {
            case "ict_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.ICT, conversionGroup);
                break;
            case "fresh_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.NORMAL, conversionGroup);
                break;
            case "fashion_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.FASHION, conversionGroup);
                break;
            case "deal_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.DEAL, conversionGroup);
                break;
        }
    }
}

private List<ConversionGroupModel> getConversionGroup() {
    StringBuilder query = new StringBuilder();
    query.append("SELECT {PK} FROM {ConversionGroup!}");
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
    SearchResult<ConversionGroupModel> result = flexibleSearchService.search(flexibleSearchQuery);
    if (result != null) {
        if (CollectionUtils.isNotEmpty(result.getResult())) {
            return result.getResult();
        }
    }
    return ListUtils.EMPTY_LIST;
}

enum ProductCategoryType
{
    ICT, FASHION, DEAL, NORMAL
}
