package productMedia

import com.vingroup.rocket.core.model.DisplayCategoryModel
import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.media.MediaModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.mediaconversion.model.ConversionGroupModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.variants.model.VariantProductModel
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.ListUtils
import org.apache.commons.lang.StringUtils

/**
 * Created by Khoi Nguyen on 6/22/2017.
 */

/*
*
* select count(*) from {ProcessedProductJobItem} where {action} = 'GROOVY_SET_MEDIA_CONVERSION_GROUP' and {reason} not in ('ALREADY_SET', 'NO_MEDIAS')
*
* */

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
categoryService = spring.getBean "categoryService"
productCategoryTypeMap = new HashMap<>()
conversionGroupModelMap = new HashMap<>()


stagedProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Staged")

productCatalogVersionOnline = 8796093186649
productCatalogVersionStaged = 8796093153881
CATALOG_VERSION = productCatalogVersionStaged
ACTION = 'GROOVY_SET_MEDIA_CONVERSION_GROUP'

def findProducts() {
    def queryStr = "select {product} from {ProcessedProductJobItem} where {action} = 'GROOVY_SET_MEDIA_CONVERSION_GROUP' and {reason} = 'ALREADY_SET'" ;

    def query = new FlexibleSearchQuery(queryStr)

    println query;
//    query.count = 2
    flexibleSearchService.search(query).result
}

def log(message) {
    //println message;
//    log.info(message)
}

products = findProducts()

reviewProducts = []

products.each {
    def product = it;
    def productCode = product.code
    log("SET MEDIA CONVERSION GROUP " + productCode)

    reason = "";
    galleryImages = product.galleryImages
    galleryImages.each {
        if (!String.valueOf(it.catalogVersion.pk).equals(String.valueOf(CATALOG_VERSION))) {
            reviewProducts.add(product)
        }
        /*if (it.conversionGroup != null) {

            medias = it.medias
            medias.each {

                if (it.internalURL != 'replicated273654712') {
                    url = it.getURL()
                    println url
                    newUrl = url.replaceAll("//cdn02.static-adayroi.com", '\\$config-adayroiLegacyProductMediaResource')
                    println ";" + it.code + ";;;" + newUrl
                }

            }
        }*/
    }

}


myset = reviewProducts.toSet()
myset.each {
    println it.code
    /*medias = findMediaByCode(it.code)
    it.galleryImages = medias;
    try {
        modelService.save(it)
    } catch (Exception e) {
        println e
    }*/


}


def findMediaByCode(code) {
    primaryImage = findExtractMediaByCode(code)
    if (primaryImage != null) {
        relevantMedias = new ArrayList(findRelevantMediasByCode(code))
        relevantMedias.add(0, primaryImage)
        return relevantMedias
    }
}

def findExtractMediaByCode(code) {
    queryStr = "SELECT {PK} FROM {MediaContainer} WHERE {qualifier} = '" + code + "' AND {catalogversion} = " + CATALOG_VERSION
    query = new FlexibleSearchQuery(queryStr)
    result = flexibleSearchService.search(query)
    if (result.getResult().size() > 0) {
        return result.getResult().get(0)
    }
}

def findRelevantMediasByCode(code) {
    queryStr = "SELECT {PK} FROM {MediaContainer} WHERE {qualifier} like '" + code + "\\_%' escape '\\' AND {catalogversion} = " + CATALOG_VERSION
    query = new FlexibleSearchQuery(queryStr)
    result = flexibleSearchService.search(query)
    return result.getResult()
}
