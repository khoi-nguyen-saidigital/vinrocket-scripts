package productMedia

import com.vingroup.rocket.core.model.DisplayCategoryModel
import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.media.MediaModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.mediaconversion.enums.ConversionStatus
import de.hybris.platform.mediaconversion.model.ConversionGroupModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.variants.model.VariantProductModel
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.ListUtils
import org.apache.commons.lang.StringUtils

/**
 * Created by Khoi Nguyen on 6/22/2017.
 */

/*
*
* select count(*) from {ProcessedProductJobItem} where {action} = 'GROOVY_SET_MEDIA_CONVERSION_GROUP' and {reason} not in ('ALREADY_SET', 'NO_MEDIAS')
*
*
* select * from {ProcessedProductJobItem} where {action} = 'GROOVY_CONVERT_MEDIA' AND {reason} not in ('CONVERTED_ALREADY', 'NO_CONVERSION_GROUP')
* select * from {ProcessedProductJobItem} where {action} = 'GROOVY_CONVERT_MEDIA' AND {reason} in ('ERROR | null', 'NO_CONVERSION_GROUP')
*
*
* */

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
categoryService = spring.getBean "categoryService"
productCategoryTypeMap = new HashMap<>()
conversionGroupModelMap = new HashMap<>()

PREVIOUS_ACTION = 'GROOVY_UPDATE_MEDIA_CONVERSION_GROUP_PRODUCTION'
ACTION = 'GROOVY_UPDATE_CONVERT_MEDIA_PRODUCTION'

def findProducts() {
    def queryStr = "select distinct{product} from {ProcessedProductJobItem} where {action} = '" + PREVIOUS_ACTION + "' and {reason} = 'CHANGED'" +
            " AND {product} not in ({{SELECT {product} from {ProcessedProductJobItem} WHERE {action} = '" + ACTION + "'}})";



    def query = new FlexibleSearchQuery(queryStr)

    log(query);
    flexibleSearchService.search(query).result
}

def log(message) {
//    println message;
    log.info(message)
}

products = findProducts()
products.each {
    def product = it;
    def productCode = product.code
    log("GROOVY_CONVERT_MEDIA " + productCode)

    reason = "";

    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel()
    item.product = product;
    item.productCode = productCode
    item.action = ACTION

    int needConverting = 0;
    try {
        galleryImages = product.galleryImages
        galleryImages.each {
            conversionGroup = it.conversionGroup
            mediaContainer = it
            if (conversionGroup != null && mediaContainer.getConversionStatus() == ConversionStatus.UNCONVERTED) {
                supportedFormats = conversionGroup.getSupportedFormats()
                supportedFormats.each {
                    mediaConversionService.convertInTransaction(mediaContainer, it)
                }

                needConverting++
            } else if (it.conversionGroup == null) {
                reason = "NO_CONVERSION_GROUP"
            } else if (it.getConversionStatus() == ConversionStatus.CONVERTED) {
                reason = "CONVERTED_ALREADY"
            } else if (it.getConversionStatus() == ConversionStatus.PARTIALLY_CONVERTED) {
                supportedFormats = conversionGroup.getSupportedFormats()
                supportedFormats.each {
                    mediaConversionService.convertInTransaction(mediaContainer, it)
                }

                needConverting ++
            } else {
                reason = "OTHER"
            }

        }

        if (needConverting > 0) {
            reason = String.valueOf(needConverting)
        }


    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg != null && errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason = "ERROR | " + errorMsg
        log(e)
    }
    item.reason = reason;
    modelService.save(item)
    log("FINISH GROOVY_CONVERT_MEDIA for product [" + productCode + "] with reason [" + reason + "]")
}