package cleanup

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery

/**
 * Created by Khoi Nguyen on 6/23/2017.
 */
flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"

def findErrorItems() {
    queryStr = "select {pk} from {ProcessedProductJobItem} where {action} = 'GROOVY_FILLING_GALLERY_IMAGE' and {reason} like 'ERROR | Dummy article is missing%'"

    query = new FlexibleSearchQuery(queryStr)

    println query;
    flexibleSearchService.search(query).result
}

items = findErrorItems()
items.each {
    println it.productCode
    modelService.remove(it)
}
