import com.vingroup.rocket.core.model.DisplayCategoryModel
import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.mediaconversion.enums.ConversionStatus
import de.hybris.platform.mediaconversion.model.ConversionGroupModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.variants.model.VariantProductModel
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.ListUtils
import org.apache.commons.io.IOUtils
import org.apache.commons.lang.StringUtils
import org.apache.commons.io.IOUtils


/**
 * Created by Khoi Nguyen on 6/22/2017.
 */

/*
*
* select * from {ProcessedProductJobItem} where {action} = 'GROOVY_FILLING_GALLERY_IMAGE' and {reason} not in ('ERROR', 'IGNORE', 'NOT_FOUND_MEDIAS')
*
* */

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
sessionService = spring.getBean "sessionService"
categoryService = spring.getBean "categoryService"
mediaService = spring.getBean "mediaService"


productCategoryTypeMap = new HashMap<>()
conversionGroupModelMap = new HashMap<>()


onlineProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Online")

ACTION = 'UPDATING_PRODUCTS_20170815_1'

def log(message) {
//    println message
    log.info(message)
}


initializeConfiguration()


def findProducts() {
    result = [];
    listPks = []

    processedItems = getProcessedItems()

    media = mediaService.getMedia(ACTION)
    println media
    content = mediaService.getDataFromMedia(media)
    String s = new String(content)
    lines = IOUtils.readLines(new StringReader(s))
    lines.each {
        if (StringUtils.isNotEmpty(it)) {
            productCode = it.trim();
            if (!productCode.startsWith("PRI")) {
                productCode = "dv_" + productCode
            }
            product = productService.getProductForCode(onlineProductCatalog, productCode)

            if (!processedItems.contains(product)) {
                result.add(product)
            }

        }
    }

    return result;
}

def getProcessedItems() {
    query = "SELECT {product} FROM {ProcessedProductJobItem} where {action} = '" + ACTION + "'"
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query)
    return flexibleSearchService.search(flexibleSearchQuery).result
}



products = findProducts()
log("START PROCESS MEDIA CONVERSION FOR " + products.size() + " PRODUCTS")

products.each {
    product = it;
    productCode = product.code
    log("PROCESS MEDIA CONVERSION FOR " + productCode)

    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel()
    item.setProduct(product)
    item.setProductCode(productCode)
    item.setAction(ACTION)

    reason_find_gallery = ""
    reason_conversion_group = ""
    reason_convert_media = ""
    reason = "";
    originalMedias = product.galleryImages;
    if (originalMedias != null && originalMedias.size() > 0) {
        reason_find_gallery = "PRODUCT_HAS_GALLERY"
    } else {
        reason_find_gallery = "NOT_FOUND_MEDIAS"
    }




    reason_conversion_group = setMediaConversionGroup(product)

    reason_convert_media = convertMedia(product)


    reason = reason_find_gallery + "|" + reason_conversion_group + "|" + reason_convert_media
    message = "FINISH MEDIA CONVERSION JOB FOR PRODUCT [" + productCode + "] WITH REASON [" + reason + "]"

    log(message )
    item.setReason(reason)
    modelService.save(item)

}

def setMediaConversionGroup(product) {
    reason_conversion_group = ""
    try {
        List<MediaContainerModel> galleryImages = product.getGalleryImages();
        if (CollectionUtils.isEmpty(galleryImages)) {
            reason_conversion_group = "NO_MEDIAS"
        } else {
            ProductCategoryType productType = getCategoryTypeFromProduct(product);
            ConversionGroupModel conversionGroup = conversionGroupModelMap.get(productType);
            reason_conversion_group = productType.name()
            for (MediaContainerModel mediaContainer : galleryImages) {
                mediaContainer.setConversionGroup(conversionGroup);
                modelService.save(mediaContainer);
            }
        }

    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason_conversion_group = "ERROR - " + errorMsg
        log(e)
    }
    return reason_conversion_group
}

def convertMedia(product) {
    int needConverting = 0;
    reason_convert_media = ""

    try {
        galleryImages = product.galleryImages
        galleryImages.each {
            conversionGroup = it.conversionGroup
            mediaContainer = it
            if (conversionGroup != null && mediaContainer.getConversionStatus() == ConversionStatus.UNCONVERTED) {
                supportedFormats = conversionGroup.getSupportedFormats()
                supportedFormats.each {
                    mediaConversionService.getOrConvert(mediaContainer, it)
                }
                needConverting++
            } else if (it.conversionGroup == null) {
                reason_convert_media = "NO_CONVERSION_GROUP"
            } else if (it.getConversionStatus() == ConversionStatus.CONVERTED) {
                reason_convert_media = "CONVERTED_ALREADY"
            } else if (it.getConversionStatus() == ConversionStatus.PARTIALLY_CONVERTED) {
                supportedFormats = conversionGroup.getSupportedFormats()
                supportedFormats.each {
                    mediaConversionService.getOrConvert(mediaContainer, it)
                }
                needConverting ++
            } else {
                reason_convert_media = "OTHER"
            }

        }

        if (needConverting > 0) {
            reason_convert_media = String.valueOf(needConverting)
        }


    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg != null && errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason_convert_media = "ERROR - " + errorMsg
        log(e)
    }

    return reason_convert_media
}



private ProductCategoryType getCategoryTypeFromProduct(ProductModel productModel) {
    Collection<CategoryModel> categories = CollectionUtils.EMPTY_COLLECTION;
    if (productModel instanceof VariantProductModel) {
        categories = ((VariantProductModel) productModel).getBaseProduct().getSupercategories();
    } else if (productModel instanceof ProductModel) {
        categories = productModel.getSupercategories();
    }

    if (CollectionUtils.isNotEmpty(categories)) {
        for (CategoryModel displayCategory : categories) {
            if (displayCategory != null && displayCategory instanceof DisplayCategoryModel) {
                if (getCategoryTypeFromCategory(displayCategory) != null) {
                    return getCategoryTypeFromCategory(displayCategory);
                }
                Collection<CategoryModel> superCategories = categoryService.getAllSupercategoriesForCategory(displayCategory);
                if (CollectionUtils.isNotEmpty(superCategories)) {
                    for (CategoryModel category : superCategories) {
                        if (getCategoryTypeFromCategory(category) != null) {
                            return getCategoryTypeFromCategory(category);
                        }
                    }
                }

            }
        }
    }

    return ProductCategoryType.NORMAL;
}

private ProductCategoryType getCategoryTypeFromCategory(CategoryModel category) {
    if (category != null && category instanceof DisplayCategoryModel) {
        return productCategoryTypeMap.get(category.getCode());
    }

    return null;
}



private initializeConfiguration() {
    String fashionCategories = "1,780,842"
    initProductCategoryType(fashionCategories, ProductCategoryType.FASHION);
    String ictCategories = "321"
    initProductCategoryType(ictCategories, ProductCategoryType.ICT);
    String dealCategories = "332552,332555,332550,332551"
    initProductCategoryType(dealCategories, ProductCategoryType.DEAL);

    List<ConversionGroupModel> conversionGroups = getConversionGroup()
    for (ConversionGroupModel conversionGroup : conversionGroups) {
        switch (conversionGroup.getCode()) {
            case "ict_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.ICT, conversionGroup);
                break;
            case "fresh_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.NORMAL, conversionGroup);
                break;
            case "fashion_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.FASHION, conversionGroup);
                break;
            case "deal_product_image_conversion_group":
                conversionGroupModelMap.put(ProductCategoryType.DEAL, conversionGroup);
                break;
        }
    }
}

private void initProductCategoryType(String categoryStr, ProductCategoryType type) {
    if (StringUtils.isNotEmpty(categoryStr)) {
        String[] categories = categoryStr.split(",");
        for (String categoryCode : categories) {
            productCategoryTypeMap.put(categoryCode, type);
        }
        log("Init for product category type: " + type + "[" + categoryStr + "]");
    }
}

private List<ConversionGroupModel> getConversionGroup() {
    StringBuilder query = new StringBuilder();
    query.append("SELECT {PK} FROM {ConversionGroup!}");
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
    SearchResult<ConversionGroupModel> result = flexibleSearchService.search(flexibleSearchQuery);
    if (result != null) {
        if (CollectionUtils.isNotEmpty(result.getResult())) {
            return result.getResult();
        }
    }
    return ListUtils.EMPTY_LIST;
}

enum ProductCategoryType
{
    ICT, FASHION, DEAL, NORMAL
}
