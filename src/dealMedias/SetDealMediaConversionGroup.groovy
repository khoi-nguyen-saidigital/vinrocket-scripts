package dealMedias
//SetDealMediaConversionGroup
import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableSet
import com.vingroup.rocket.core.model.ProcessedProductJobItemModel
import de.hybris.platform.mediaconversion.model.ConversionGroupModel
import de.hybris.platform.servicelayer.interceptor.impl.InterceptorExecutionPolicy
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.servicelayer.session.SessionExecutionBody
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.ListUtils

/**
 * Created by Khoi Nguyen on 6/22/2017.
 */

/*
*
* select * from {ProcessedProductJobItem} where {action} = 'GROOVY_FILLING_GALLERY_IMAGE' and {reason} not in ('ERROR', 'IGNORE', 'NOT_FOUND_MEDIAS')
*
* */

flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"
catalogVersionService = spring.getBean "catalogVersionService"
productService = spring.getBean "productService"
sessionService = spring.getBean "sessionService"

stagedProductCatalog = catalogVersionService.getCatalogVersion("adayroiProductCatalog", "Staged")

CATALOG_VERSION = stagedProductCatalog.pk
ACTION = 'GROOVY_DEAL_MENU_SET_CONVERSION_GROUP'

def findProducts() {
    queryStr = "select distinct{mo.pk} from {MerchantOffer as mo JOIN EnumerationValue as s ON {mo.approvalStatus} = {s.pk}} where {menudetailsimages} is not null AND {mo.catalogVersion} = '"+CATALOG_VERSION+"' AND {mo.pk} not in ({{ select {product} FROM {processedproductjobitem} where {action} = 'GROOVY_DEAL_MENU_SET_CONVERSION_GROUP' AND {reason} != 'NOT_FOUND_MEDIAS' }})"
    query = new FlexibleSearchQuery(queryStr)
    log(query);
    query.count = 10000
    flexibleSearchService.search(query).result
}

imageConversionGroup = getConversionGroup(MENU_TYPE.IMAGE)
pdfConversionGroup = getConversionGroup(MENU_TYPE.PDF)

pdfFormat = flexibleSearchService.search("SELECT {PK} FROM {ConversionMediaFormat} WHERE {qualifier} = 'deal_menu_details_pdf_format'").result.get(0)

products = findProducts()
products.each {
    product = it;
    productCode = product.code
    log("GROOVY_DEAL_MENU_SET_CONVERSION_GROUP FOR PRODUCT " + productCode)

    ProcessedProductJobItemModel item = new ProcessedProductJobItemModel()
    item.setProduct(product)
    item.setProductCode(productCode)
    item.setAction(ACTION)

    try {
        reason = "";
        menuDetailsImages = product.menuDetailsImages;
        if (menuDetailsImages != null && menuDetailsImages.size() > 0) {
            menuDetailsImages.each {
                menu = it;
                if (menu.medias != null && menu.medias.size() > 0) {
                    firstMedia = menu.medias.get(0)
                    mime = firstMedia.mime
                    if (mime != null) {
                        if (mime.equals("application/pdf")) {
                            menu.medias.each {
                                if (it.mediaFormat == null) {
                                    it.mediaFormat = pdfFormat
                                    modelService.save(it)
                                }
                            }
                            menu.setConversionGroup(null)
                            reason = "PDF"
                            modelService.save(menu)
                        } else if (mime.contains("image")) {
                            menu.setConversionGroup(imageConversionGroup)
                            reason = reason + "IMAGE"
                            modelService.save(menu)
                        } else {
                            reason = "WRONG_MIME"
                        }
                    } else {
                        reason = "NOT_FOUND_MIME"
                    }
                } else {
                    reason = "NOT_FOUND_MEDIAS"
                }
            }
        } else {
            reason = "NO_MENU"
        }

    } catch (Exception e) {
        String errorMsg = e.getMessage();
        if (errorMsg.length() > 200) {
            errorMsg = errorMsg.substring(0, 200)
        }
        reason = "ERROR | " + errorMsg
        log(e)
    }


    message = "FINISH GROOVY_DEAL_MENU_SET_CONVERSION_GROUP FOR product [" + productCode + "] with reason [" + reason + "]"
    log(message )
    item.setReason(reason)
    modelService.save(item)

}


def log(message) {
    log.info(message)
    //println message
}


private ConversionGroupModel getConversionGroup(MENU_TYPE type) {
    code = ""
    if (type == MENU_TYPE.PDF) {
        code = "deal_menu_details_pdf_conversion_group"
    } else if (type == MENU_TYPE.IMAGE) {
        code = "deal_menu_details_images_conversion_group"
    } else {
        return null;
    }

    StringBuilder query = new StringBuilder();
    query.append("SELECT {PK} FROM {ConversionGroup!} WHERE {code} = ?code");
    FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
    flexibleSearchQuery.addQueryParameter("code", code)

    SearchResult<ConversionGroupModel> result = flexibleSearchService.search(flexibleSearchQuery);
    if (result != null) {
        if (CollectionUtils.isNotEmpty(result.getResult())) {
            return result.getResult().get(0);
        }
    }
    return null
}

enum MENU_TYPE {
    PDF, IMAGE
}
