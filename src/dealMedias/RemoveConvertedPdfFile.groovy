package dealMedias

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery


flexibleSearchService = spring.getBean "flexibleSearchService"
mediaConversionService = spring.getBean "mediaConversionService"
modelService = spring.getBean "modelService"

medias = getMedias()
medias.each {
    media = it;
    mediaFormat = media.mediaFormat
    original = media.original
    container = media.mediaContainer
    println container.qualifier

    original.mediaFormat = mediaFormat
    container.conversionGroup = null

    modelService.remove(media)
    modelService.save(original)
    modelService.save(container)
}


def getMedias() {
    query = "select {pk} from {Media} where {mediaformat} = 8796159115315 and {catalogversion} = 8796093153881 and {original} is not null"
    flexibleQuery = new FlexibleSearchQuery(query)
    flexibleQuery.count = 1
    flexibleSearchService.search(flexibleQuery).result
}